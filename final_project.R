setwd("~/������ ������ ����/��� �/����� �/����� �����/������")
project.raw <- read.csv('projectDB.csv')

project <- project.raw

table(project.raw$class)

summary(project)
str(project)

dim(project.raw)

#target column
project$class <- as.factor(project$class)

#install.packages('ggplot2')
library(ggplot2)

#prepering data

#Check correlation between specific column and the target column

#coerce

clean <- function(x,beater){
  if (x>beater) return(beater)
  return(x)
}

cleanU <- function(x,beater){
  if (x<beater) return(beater)
  return(x)
}

#Attr1
str(project$Attr1)
project$Attr1 <- as.integer(project$Attr1)
summary(project$Attr1)
ggplot(project, aes(class ,Attr1)) + geom_boxplot()


#Attr2
str(project$Attr2)
project$Attr2 <- as.integer(project$Attr2)
summary(project$Attr2)
ggplot(project, aes(class ,Attr2)) + geom_boxplot()


#Attr3
str(project$Attr3)
project$Attr3 <- as.integer(project$Attr3)
summary(project$Attr3)
ggplot(project, aes(class ,Attr3)) + geom_boxplot()


#Attr4
str(project$Attr4)
project$Attr4 <- as.integer(project$Attr4)
summary(project$Attr4)
ggplot(project, aes(class ,Attr4)) + geom_boxplot()


#Attr5
str(project$Attr5)
project$Attr5 <- as.integer(project$Attr4)
summary(project$Attr5)
ggplot(project, aes(class ,Attr5)) + geom_boxplot()

#Attr6
str(project$Attr6)
project$Attr6 <- as.integer(project$Attr4)
summary(project$Attr6)
ggplot(project, aes(class ,Attr6)) + geom_boxplot()

#Attr7
str(project$Attr7)
project$Attr7 <- as.integer(project$Attr7)
summary(project$Attr7)
ggplot(project, aes(class ,Attr7)) + geom_boxplot()

#Attr8
str(project$Attr8)
project$Attr8 <- as.integer(project$Attr8)
summary(project$Attr8)
ggplot(project, aes(class ,Attr8)) + geom_boxplot()

#Attr9- 
#No conection
str(project$Attr9)
project$Attr9 <- as.integer(project$Attr9)
summary(project$Attr9)
ggplot(project, aes(class ,Attr9)) + geom_boxplot()

#Attr10
str(project$Attr10)
project$Attr10 <- as.integer(project$Attr10)
summary(project$Attr10)
ggplot(project, aes(class ,Attr10)) + geom_boxplot()

#Attr11
str(project$Attr11)
project$Attr11 <- as.integer(project$Attr11)
summary(project$Attr11)
ggplot(project, aes(class ,Attr11)) + geom_boxplot()

#Attr12
str(project$Attr12)
project$Attr12 <- as.integer(project$Attr12)
summary(project$Attr12)
ggplot(project, aes(class ,Attr12)) + geom_boxplot()

#Attr13- with cleaning extream low and hi
str(project$Attr13)
project$Attr13 <- project.raw$Attr13
summary(project$Attr13)
ggplot(project, aes(x=Attr13)) + geom_histogram() + xlim(-1,1)
project$Attr13 <- sapply(project$Attr13,clean, beater=0.5)
project$Attr13 <- sapply(project$Attr13,cleanU, beater=-0.5)
ggplot(project, aes(class ,Attr13)) + geom_boxplot()

#Attr14
str(project$Attr14)
project$Attr14 <- as.integer(project$Attr14)
summary(project$Attr14)
ggplot(project, aes(class ,Attr14)) + geom_boxplot()

#Attr15
str(project$Attr15)
project$Attr15 <- as.integer(project$Attr15)
summary(project$Attr15)
ggplot(project, aes(class ,Attr15)) + geom_boxplot()

#Attr16
str(project$Attr16)
project$Attr16 <- as.integer(project$Attr16)
summary(project$Attr16)
ggplot(project, aes(class ,Attr16)) + geom_boxplot()

#Attr17
str(project$Attr17)
project$Attr17 <- as.integer(project$Attr17)
summary(project$Attr17)
ggplot(project, aes(class ,Attr17)) + geom_boxplot()

#Attr18
str(project$Attr18)
project$Attr18 <- as.integer(project$Attr18)
summary(project$Attr18)
ggplot(project, aes(class ,Attr18)) + geom_boxplot()

#Attr19- with cleaning extream low and hi
str(project$Attr19)
summary(project$Attr19)
ggplot(project, aes(x=Attr19)) + geom_histogram() + xlim(-1,1)
project$Attr19 <- sapply(project$Attr19,clean, beater=0.5)
project$Attr19 <- sapply(project$Attr19,cleanU, beater=-0.5)
ggplot(project, aes(class ,Attr19)) + geom_boxplot()

#Attr20- with cleaning extream low and hi
#No conection
str(project$Attr20)
summary(project$Attr20)
ggplot(project, aes(x=Attr20)) + geom_histogram() + xlim(0,300)
project$Attr20 <- sapply(project$Attr20,clean, beater=200)
project$Attr20 <- sapply(project$Attr20,cleanU, beater=0)
ggplot(project, aes(class ,Attr20)) + geom_boxplot()

#Attr21
str(project$Attr21)
project$Attr21 <- as.integer(project$Attr21)
summary(project$Attr21)
ggplot(project, aes(class ,Attr21)) + geom_boxplot()

#Attr22
str(project$Attr22)
project$Attr22 <- as.integer(project$Attr22)
summary(project$Attr22)
ggplot(project, aes(class ,Attr22)) + geom_boxplot()

#Attr23- with cleaning extream low and hi
str(project$Attr23)
summary(project$Attr23)
ggplot(project, aes(x=Attr23)) + geom_histogram() + xlim(-1,0.8)
project$Attr23 <- sapply(project$Attr23,clean, beater=0.5)
project$Attr23 <- sapply(project$Attr23,cleanU, beater=-0.5)
ggplot(project, aes(class ,Attr23)) + geom_boxplot()

#Attr24
str(project$Attr24)
project$Attr24 <- as.integer(project$Attr24)
summary(project$Attr24)
ggplot(project, aes(class ,Attr24)) + geom_boxplot()

#Attr25
str(project$Attr25)
project$Attr25 <- as.integer(project$Attr25)
summary(project$Attr25)
ggplot(project, aes(class ,Attr25)) + geom_boxplot()

#Attr26
str(project$Attr26)
project$Attr26 <- as.integer(project$Attr26)
summary(project$Attr26)
ggplot(project, aes(class ,Attr26)) + geom_boxplot()

#Attr27
str(project$Attr27)
project$Attr27 <- as.integer(project$Attr27)
summary(project$Attr27)
ggplot(project, aes(class ,Attr27)) + geom_boxplot()

#Attr28
str(project$Attr28)
project$Attr28 <- as.integer(project$Attr28)
summary(project$Attr28)
ggplot(project, aes(class ,Attr28)) + geom_boxplot()

#Attr29
str(project$Attr29)
project$Attr29 <- as.integer(project$Attr29)
summary(project$Attr29)
ggplot(project, aes(class ,Attr29)) + geom_boxplot()

#Attr30
str(project$Attr30)
summary(project$Attr30)
ggplot(project, aes(x=project$Attr30)) + geom_histogram() + xlim(-0.5,0.7)
project$Attr30 <- sapply(project$Attr30,clean, beater=0.5)
project$Attr30 <- sapply(project$Attr30,cleanU, beater=-0.12)
ggplot(project, aes(class ,Attr30)) + geom_boxplot()

#Attr31
str(project$Attr31)
summary(project$Attr31)
ggplot(project, aes(x=project$Attr31)) + geom_histogram() + xlim(-1,1)
project$Attr31 <- sapply(project$Attr31,clean, beater=0.025)
project$Attr31 <- sapply(project$Attr31,cleanU, beater=-0.025)
ggplot(project, aes(class ,Attr31)) + geom_boxplot()

#Attr32
#no connetion
str(project$Attr32)
project$Attr32 <- as.integer(project$Attr32)
summary(project$Attr32)
ggplot(project, aes(class ,Attr32)) + geom_boxplot()

#Attr33
str(project$Attr33)
project$Attr33 <- as.integer(project$Attr33)
summary(project$Attr33)
ggplot(project, aes(class ,Attr33)) + geom_boxplot()

#Attr34
#no connection
str(project$Attr34)
project$Attr34 <- as.integer(project$Attr34)
summary(project$Attr34)
ggplot(project, aes(class ,Attr34)) + geom_boxplot()

#Attr35
str(project$Attr35)
project$Attr35 <- as.integer(project$Attr35)
summary(project$Attr35)
ggplot(project, aes(class ,Attr35)) + geom_boxplot()

#Attr36
#no connection
str(project$Attr36)
project$Attr36 <- as.integer(project$Attr36)
summary(project$Attr36)
ggplot(project, aes(class ,Attr36)) + geom_boxplot()

#Attr37
#no connection
str(project$Attr37)
project$Attr37 <- as.integer(project$Attr37)
summary(project$Attr37)
ggplot(project, aes(class ,Attr37)) + geom_boxplot()

#Attr38
str(project$Attr38)
project$Attr38 <- as.integer(project$Attr38)
summary(project$Attr38)
ggplot(project, aes(class ,Attr38)) + geom_boxplot()

#Attr39
str(project$Attr39)
summary(project$Attr39)
ggplot(project, aes(x=project$Attr39)) + geom_histogram() + xlim(-0.2,0.2)
project$Attr39 <- sapply(project$Attr39,clean, beater=0.125)
project$Attr39 <- sapply(project$Attr39,cleanU, beater=-0.03)
ggplot(project, aes(class ,Attr39)) + geom_boxplot()

#Attr40
str(project$Attr40)
project$Attr40 <- as.integer(project$Attr40)
summary(project$Attr40)
ggplot(project, aes(class ,Attr40)) + geom_boxplot()

#Attr41
str(project$Attr41)
project$Attr41 <- as.integer(project$Attr41)
summary(project$Attr41)
ggplot(project, aes(class ,Attr41)) + geom_boxplot()

#Attr42
str(project$Attr42)
summary(project$Attr42)
ggplot(project, aes(x=project$Attr42)) + geom_histogram() + xlim(-1,1)
project$Attr42 <- sapply(project$Attr42,clean, beater=0.25)
project$Attr42 <- sapply(project$Attr42,cleanU, beater=-0.1)
ggplot(project, aes(class ,Attr42)) + geom_boxplot()

#Attr43
#no connection
str(project$Attr43)
summary(project$Attr43)
ggplot(project, aes(x=project$Attr43)) + geom_histogram() + xlim(-10,300)
project$Attr43 <- sapply(project$Attr43,clean, beater=200)
project$Attr43 <- sapply(project$Attr43,cleanU, beater=0)
ggplot(project, aes(class ,Attr43)) + geom_boxplot()

#Attr44
#no connection
str(project$Attr44)
summary(project$Attr44)
ggplot(project, aes(x=project$Attr44)) + geom_histogram() + xlim(-20,300)
project$Attr44 <- sapply(project$Attr44,clean, beater=150)
project$Attr44 <- sapply(project$Attr44,cleanU, beater=0)
ggplot(project, aes(class ,Attr44)) + geom_boxplot()

#Attr46
str(project$Attr46)
project$Attr46 <- as.integer(project$Attr46)
summary(project$Attr46)
ggplot(project, aes(class ,Attr46)) + geom_boxplot()

#Attr47
#no connection
str(project$Attr47)
project$Attr47 <- as.integer(project$Attr47)
summary(project$Attr47)
ggplot(project, aes(class ,Attr47)) + geom_boxplot()

#Attr48
#no connection
str(project$Attr48)
project$Attr48 <- as.integer(project$Attr48)
summary(project$Attr48)
ggplot(project, aes(class ,Attr48)) + geom_boxplot()

#Attr49
str(project$Attr49)
summary(project$Attr49)
ggplot(project, aes(x=project$Attr49)) + geom_histogram() + xlim(-0.5,0.5)
project$Attr49 <- sapply(project$Attr49,clean, beater=0.125)
project$Attr49 <- sapply(project$Attr49,cleanU, beater=-0.125)
ggplot(project, aes(class ,Attr49)) + geom_boxplot()

#Attr50
str(project$Attr50)
project$Attr50 <- as.integer(project$Attr50)
summary(project$Attr50)
ggplot(project, aes(class ,Attr50)) + geom_boxplot()

#Attr51
str(project$Attr51)
project$Attr51 <- as.integer(project$Attr51)
summary(project$Attr51)
ggplot(project, aes(class ,Attr51)) + geom_boxplot()

#Attr52
str(project$Attr52)
project$Attr52 <- as.integer(project$Attr52)
summary(project$Attr52)
ggplot(project, aes(class ,Attr52)) + geom_boxplot()

#Attr53
str(project$Attr53)
project$Attr53 <- as.integer(project$Attr53)
summary(project$Attr53)
ggplot(project, aes(class ,Attr53)) + geom_boxplot()

#Attr54
str(project$Attr54)
project$Attr54 <- as.integer(project$Attr54)
summary(project$Attr54)
ggplot(project, aes(class ,Attr54)) + geom_boxplot()

#Attr55- with cleaning extream low and hi
str(project$Attr55)
summary(project$Attr55)
ggplot(project, aes(x=Attr55)) + geom_histogram() + xlim(-9000,22000)
project$Attr55 <- sapply(project$Attr55,clean, beater=22000)
project$Attr55 <- sapply(project$Attr55,cleanU, beater=-8000)
ggplot(project, aes(class ,Attr55)) + geom_boxplot()

#Attr56- with cleaning extream low and hi
#NO conection
str(project$Attr56)
summary(project$Attr56)
ggplot(project, aes(x=Attr56)) + geom_histogram() + xlim(-0,2)
project$Attr56 <- sapply(project$Attr56,clean, beater=22000)
project$Attr56 <- sapply(project$Attr56,cleanU, beater=-0.8)
ggplot(project, aes(class ,Attr56)) + geom_boxplot()

#Attr57
str(project$Attr57)
project$Attr57 <- as.integer(project$Attr57)
summary(project$Attr57)
ggplot(project, aes(class ,Attr57)) + geom_boxplot()

#Attr58- with cleaning extream low and hi
#NO conection
str(project$Attr58)
summary(project$Attr58)
ggplot(project, aes(x=Attr58)) + geom_histogram() + xlim(-0.2,2)
project$Attr58 <- sapply(project$Attr58,clean, beater=1.5)
project$Attr58 <- sapply(project$Attr58,cleanU, beater=0)
ggplot(project, aes(class ,Attr58)) + geom_boxplot()

#Attr59
#NO conection
str(project$Attr59)
project$Attr59 <- as.integer(project$Attr59)
summary(project$Attr59)
ggplot(project, aes(class ,Attr59)) + geom_boxplot()

#Attr60
#NO conection
str(project$Attr60)
project$Attr60 <- as.integer(project$Attr60)
summary(project$Attr60)
ggplot(project, aes(class ,Attr60)) + geom_boxplot()

#Attr61
#NO conection
str(project$Attr61)
project$Attr61 <- as.integer(project$Attr61)
summary(project$Attr61)
ggplot(project, aes(class ,Attr61)) + geom_boxplot()

#Attr62- with cleaning extream low and hi
str(project$Attr62)
summary(project$Attr62)
ggplot(project, aes(x=Attr62)) + geom_histogram() + xlim(0,250)
project$Attr62 <- sapply(project$Attr62,clean, beater=250)
project$Attr62 <- sapply(project$Attr62,cleanU, beater=0)
ggplot(project, aes(class ,Attr62)) + geom_boxplot()

#Attr63
str(project$Attr63)
project$Attr63 <- as.integer(project$Attr63)
summary(project$Attr63)
ggplot(project, aes(class ,Attr63)) + geom_boxplot()

#Attr64
#NO conection
str(project$Attr64)
project$Attr64 <- as.integer(project$Attr64)
summary(project$Attr64)
ggplot(project, aes(class ,Attr64)) + geom_boxplot()


#Return to the original data and change all the "?" to NA

project.raw <-read.csv('projectDB.csv', header=TRUE, na.strings = "?")
project<-project.raw


#check how many NA's in each column
summary(is.na.data.frame(project))

#discared Attr37 because there is a lot of NA's (2548)
project$Attr37 <- NULL

#Check correlation table to see if some columns can be removed

project.cor <- cor(project[1:(ncol(project)-1)], use = "complete.obs")
project.cor

#install.packages("corrplot")
library(corrplot)

corrplot(project.cor, type="upper")

#install.packages("caret")
library(caret)

hc <- findCorrelation(project.cor, cutoff=0.8, verbose = T)
hc = sort(hc)
hc


#these columns are recommended to be removed because of the high correlation:
# 2  3  5  7  8  9  11 12 13 15 17 18 19 20 23 24 29 31 32 36 37 38 44 45 46 
# 47 49 51 53 63 64


#discared Attr

project <- subset(project, select=-c(Attr2,Attr3,Attr5,Attr7,Attr8,Attr9,Attr11,
                                                         Attr12,Attr13,Attr15,Attr17,Attr18,Attr19,Attr20,Attr23,Attr24,Attr29,
                                                         Attr31,Attr32,Attr34,Attr36,Attr38,Attr43,Attr44,
                                                         Attr45,Attr46,Attr47,Attr48,Attr49,Attr51,Attr53,Attr56,
                                                          Attr58,Attr59,Attr60,Attr61,Attr63,Attr64))

colnames(project)

#discared id
project$id <- NULL

#target column
project$class <- as.factor(project$class)

str(project)
summary(project)

#Function that return average instead NA

make_average <- function(x,xvec){
  if(is.na(x)) return(mean(xvec, na.rm = T))
  return(x)
}

#Change all NAs to average

project$Attr1 <- sapply(project$Attr1,make_average,xvec = project$Attr1)
project$Attr4 <- sapply(project$Attr4,make_average,xvec = project$Attr4)
project$Attr6 <- sapply(project$Attr6,make_average,xvec = project$Attr6)
project$Attr10 <- sapply(project$Attr10,make_average,xvec = project$Attr10)
project$Attr14 <- sapply(project$Attr14,make_average,xvec = project$Attr14)
project$Attr16 <- sapply(project$Attr16,make_average,xvec = project$Attr16)
project$Attr21 <- sapply(project$Attr21,make_average,xvec = project$Attr21)
project$Attr22 <- sapply(project$Attr22,make_average,xvec = project$Attr22)
project$Attr25 <- sapply(project$Attr25,make_average,xvec = project$Attr25)
project$Attr26 <- sapply(project$Attr26,make_average,xvec = project$Attr26)
project$Attr27 <- sapply(project$Attr27,make_average,xvec = project$Attr27)
project$Attr28 <- sapply(project$Attr28,make_average,xvec = project$Attr28)
project$Attr30 <- sapply(project$Attr30,make_average,xvec = project$Attr30)
project$Attr33 <- sapply(project$Attr33,make_average,xvec = project$Attr33)
project$Attr35 <- sapply(project$Attr35,make_average,xvec = project$Attr35)
project$Attr39 <- sapply(project$Attr39,make_average,xvec = project$Attr39)
project$Attr40 <- sapply(project$Attr40,make_average,xvec = project$Attr40)
project$Attr41 <- sapply(project$Attr41,make_average,xvec = project$Attr41)
project$Attr42 <- sapply(project$Attr42,make_average,xvec = project$Attr42)
project$Attr50 <- sapply(project$Attr50,make_average,xvec = project$Attr50)
project$Attr52 <- sapply(project$Attr52,make_average,xvec = project$Attr52)
project$Attr54 <- sapply(project$Attr54,make_average,xvec = project$Attr54)
project$Attr55 <- sapply(project$Attr55,make_average,xvec = project$Attr55)
project$Attr57 <- sapply(project$Attr57,make_average,xvec = project$Attr57)
project$Attr62 <- sapply(project$Attr62,make_average,xvec = project$Attr62)



#-----------------------------------------------------algorithems---------------------------------------------------


#separate into training set ans test set

#install.packages('caTools')
library(caTools)

filter <- sample.split(project$class, SplitRatio = 0.7)

project.train <- subset(project, filter==T)
project.test <- subset(project, filter==F)


#--------------------Tree-----------------

#install.packages('rpart')
library(rpart)
#install.packages('rpart.plot')
library(rpart.plot)

modelT <- rpart(class~.,project.train)

rpart.plot(modelT,box.palette = "RdBu", shadow.col = "grey", nn=TRUE)

predict.prob.T <- predict(modelT, project.test)[,'1']
predict.prob.T

prediction.T <- predict.prob.T > 0.7

actual.T <- project.test$class

#confusion matrix

cf.T <- table(prediction.T,actual.T)
cf.T

precision.T <- cf.T['TRUE','1']/(cf.T['TRUE','1'] + cf.T['TRUE','0'])
recall.T <- cf.T['TRUE','1']/(cf.T['TRUE','1'] + cf.T['FALSE','1'])

precision.T # 0.7619048
recall.T # 0.5203252

#--------------------Random Forest-------------

#install.packages('randomForest')
library(randomForest)

modelRF <- randomForest(class~.,data = project.train, importance=TRUE)

predict.prob.RF <- predict(modelRF,project.test)
predict.prob.RF

predict.prob.RF<-as.numeric(predict.prob.RF)

actual.RF <- project.test$class

#confusion matrix

cf.RF <- table(predict.prob.RF,actual.RF)
cf.RF

precision.RF <- cf.RF[2,2]/(cf.RF[2,2]+cf.RF[2,1])
recall.RF <- cf.RF[2,2]/(cf.RF[2,2]+cf.RF[1,2])

precision.RF # 0.8030303
recall.RF # 0.4308943


#--------------------Base Naive---------------

#install.packages('e1071')
library(e1071)

modelBN <- naiveBayes(project.train$class ~ ., data = project.train)

predict.prob.BN <- predict(modelBN,project.test,type = 'raw')[,'1']


prediction.BN <- predict.prob.BN > 0.5

actual.BN <- project.test$class

#confusion matrix

conf_matrix.BN = table(prediction.BN, actual.BN)

conf_matrix.BN

precision.BN <- conf_matrix.BN[2,2]/(conf_matrix.BN[2,2]+conf_matrix.BN[2,1])

recall.BN <- conf_matrix.BN[2,2]/(conf_matrix.BN[2,2]+conf_matrix.BN[1,2])

precision.BN # 0.3030303
recall.BN # 0.08130081

#-----------------Regration Logistic-------------

modelLR <- glm(class~.,family = binomial(link = 'logit'), data = project.train)

predict.prob.LR <- predict(modelLR,project.test,type = 'response')

prediction.LR <- predict.prob.LR > 0.5

actual.LR <- project.test$class

#confusion matrix

conf_matrix.LR = table(prediction.LR, actual.LR)

conf_matrix.LR

precision.LR <- conf_matrix.LR[2,2]/(conf_matrix.LR[2,2]+conf_matrix.LR[2,1])

recall.LR <- conf_matrix.LR[2,2]/(conf_matrix.LR[2,2]+conf_matrix.LR[1,2])

precision.LR # 0.4545455
recall.LR # 0.08130081


#-------------------ROC chart-----------------------------

#install.packages('pROC')
library(pROC)

rocT <- roc(project.test$class,predict.prob.T, direction = ">", levels = c('1','0'))
rocRF <- roc(project.test$class,predict.prob.RF, direction = ">", levels = c('1','0'))
rocBN <- roc(project.test$class,predict.prob.BN, direction = ">", levels = c('1','0'))
rocLR <- roc(project.test$class,predict.prob.LR, direction = ">", levels = c('1','0'))


plot(rocT, col= 'red', main = 'ROC chart')
par(new=TRUE) #see the 2 line in one screen
plot(rocRF, col= 'blue', main = 'ROC chart')
par(new=TRUE) #see the 2 line in one screen
plot(rocBN, col= 'green', main = 'ROC chart')
par(new=TRUE) #see the 2 line in one screen
plot(rocLR, col= 'yellow', main = 'ROC chart')

#area under curve

auc(rocT) # 0.8457
auc(rocRF) # 0.7115
auc(rocBN) # 0.5631
auc(rocLR) # 0.537



